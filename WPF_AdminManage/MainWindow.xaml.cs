﻿using Microsoft.Extensions.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPF_AdminManage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IHttpClientFactory _httpClientFactory;
        public MainWindow(IHttpClientFactory httpClientFactory)
        {
            InitializeComponent();
            _httpClientFactory = httpClientFactory;
        }

        private async void registerBtn_Click(object sender, RoutedEventArgs e)
        {
            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("https://localhost:7165");
            HttpResponseMessage response = new HttpResponseMessage();

            var requestContent = new MultipartFormDataContent
            {
                { new StringContent(txtFirstName.Text), "FirstName" },
                { new StringContent(txtLastName.Text), "LastName" },
                { new StringContent(txtEmail.Text), "Email" },
                { new StringContent(txtDOB.Text), "DOB" },
                { new StringContent(txtGender.Text), "Gender" },
                { new StringContent(txtRole.Text), "Role" }
            };

            //HttpPost
            response = await client.PostAsync($"/api/User/Register", requestContent);

            //Verifi
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                MessageBox.Show("Register Successfully");
                response.Dispose();
            }
            else
            {
                string result = response.Content.ReadAsStringAsync().Result;
                MessageBox.Show("Register Fail");
            }
        }
    }
}
