﻿using Applications.Interfaces;
using Applications.Services;
using Applications.ViewModels.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    /*[Authorize(policy: "AuthUser")]
    [EnableCors("AllowAll")]*/
    public class ClassTrainingProgramController : ControllerBase
    {
        private readonly IClassTrainingProgramService _classTrainingProgramService;

        public ClassTrainingProgramController(IClassTrainingProgramService classTrainingProgramService)
        {
            _classTrainingProgramService = classTrainingProgramService;
        }

        [HttpGet("GetAllClassTrainingProgram")]
       // [Authorize(policy: "Admins")]
        public async Task<Response> GetAllClassTrainingProgram(int pageIndex = 0, int pageSize = 10)
        {
            return await _classTrainingProgramService.GetAllClassTrainingProgram(pageIndex, pageSize);
        }

        [HttpPost("Class/AddTrainingProgram/{classId}/{trainingProgramId}")]
        //[Authorize(policy: "Admins")]
        public async Task<IActionResult> AddTrainingProgram(Guid classId, Guid trainingProgramId)
        {
            if (ModelState.IsValid)
            {
                var result = await _classTrainingProgramService.AddTrainingProgramToClass(classId, trainingProgramId);
                if (result != null)
                {
                    return Ok("Add Success");
                }
            }
            return BadRequest("Add TrainingProgram Fail");
        }

        [HttpDelete("Class/DeleteTrainingProgram/{classId}/{trainingProgramId}")]
       // [Authorize(policy: "Admins")]
        public async Task<IActionResult> DeleTrainingProgram(Guid classId, Guid trainingProgramId)
        {
            if (ModelState.IsValid)
            {
                var result = await _classTrainingProgramService.RemoveTrainingProgramFromClass(classId, trainingProgramId);
                if (result == null)
                {
                    return Ok("Remove Success");
                }
            }
            return BadRequest("Remove TrainingProgram Fail");
        }
    }
}
