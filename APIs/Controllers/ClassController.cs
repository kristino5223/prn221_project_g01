﻿using Applications.Interfaces;
using Applications.ViewModels.Response;
using Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
/*    [Authorize(policy: "AuthUser")]
    [EnableCors("AllowAll")]*/
    public class ClassController : ControllerBase
    {
        private readonly IClassService _classServices;
        public ClassController(IClassService classServices)
        {
            _classServices = classServices;
        }

        [HttpGet("GetAllClasses")]
       // [Authorize(policy: "All")]
        public async Task<Response> GetAllClasses(int pageIndex = 0, int pageSize = 10) => await _classServices.GetAllClasses(pageIndex, pageSize);

        [HttpGet("GetClassByName/{ClassName}")]
        //[Authorize(policy: "All")]
        public async Task<Response> GetClassesByName(string ClassName, int pageIndex = 0, int pageSize = 10) => await _classServices.GetClassByName(ClassName, pageIndex, pageSize);

        [HttpGet("GetClassById/{classId}")]
        //[Authorize(policy: "All")]
        public async Task<Response> GetClassById(Guid classId) => await _classServices.GetClassById(classId);

      
    }
}