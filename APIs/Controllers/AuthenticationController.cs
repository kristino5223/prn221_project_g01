﻿using Applications.Interfaces;
using Applications.ViewModels.Response;
using Applications.ViewModels.UserViewModels;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace APIs.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AuthenticationController : Controller
{
    private readonly IUserService _userService;

    public AuthenticationController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpPost("Login")]
    public async Task<Response> Login(UserLoginViewModel userLogin) 
    {
        if(!ModelState.IsValid) return new Response(HttpStatusCode.BadRequest,"worng format");
        return await _userService.Login(userLogin);
    }
}
