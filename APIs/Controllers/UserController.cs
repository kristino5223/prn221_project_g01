﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels.Response;
using Applications.ViewModels.UserViewModels;
using Domain.Enum.RoleEnum;
using Microsoft.AspNetCore.Mvc;

namespace APIs.Controllers;

[Route("api/[controller]")]
[ApiController]
//[Authorize(policy: "AuthUser")]
public class UserController : Controller
{
    private readonly IUserService _userService;

    public UserController(IUserService userService)
    {
        _userService = userService;
    }
  
    [HttpGet("GetAllUsers")]
    //[Authorize(policy: "All")]
    public async Task<Pagination<UserViewModel>> GetAllUsers(int pageIndex = 0, int pageSize = 10) => await _userService.GetAllUsers(pageIndex, pageSize);

    [HttpGet("GetUserById/{UserId}")]
    //[Authorize(policy: "All")]
    public async Task<UserViewModel> GetUserById(Guid UserId) => await _userService.GetUserById(UserId);

    [HttpPut("UpdateUser/{UserId}")]
    //[Authorize(policy: "OnlySupperAdmin")]
    public async Task<Response> UpdateUser(Guid UserId, [FromBody] UpdateUserViewModel user) => await _userService.UpdateUser(UserId, user);

    [HttpGet("GetUserByRole/{role}")]
    //[AllowAnonymous]
    public async Task<Pagination<UserViewModel>> GetUsersByRole(Role role, int pageIndex = 0, int pageSize = 10) => await _userService.GetUsersByRole(role,pageIndex,pageSize);
  
    [HttpPost("UploadFileExcel")]
    //[Authorize(policy: "OnlySupperAdmin")]
    public async Task<Response> Import(IFormFile formFile, CancellationToken cancellationToken) => await _userService.UploadFileExcel(formFile, cancellationToken);

    [HttpGet("SearchUserByName/{name}")]
    //[Authorize(policy: "All")]
    public async Task<Pagination<UserViewModel>> SearchByName(string name, int pageIndex = 0, int pageSize = 10) => await _userService.SearchUserByName(name, pageIndex, pageSize);
    
    //[HttpGet("Filter")]
    ////[Authorize(policy: "All")]
    //public async Task<Pagination<UserViewModel>> FilterUser([FromQuery] FilterUserRequest filterUserRequest,int pageNumber = 0, int pageSize = 10) => await _userService.FilterUser(filterUserRequest,pageNumber,pageSize);
    
    [HttpPost("Register")]
    //[AllowAnonymous]
    public async Task<Response> Register([FromForm]CreateUserViewModel createUserViewModel)=> await _userService.AddUser(createUserViewModel);

    //[HttpPut("Update-Image")]
    ////[Authorize(policy: "All")]
    //public async Task<Response> UpdateImage(Guid id, string image) => await _userService.UpdateImage(id, image);

}

