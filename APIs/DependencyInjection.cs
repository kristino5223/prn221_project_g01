﻿using FluentValidation;
using APIs.Services;
using Applications.Interfaces;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Text.Json.Serialization;
using APIs.Validations.TrainingProgramValidations;
using Application.ViewModels.TrainingProgramModels;
using Applications.ViewModels.TrainingProgramModels;

namespace APIs;

public static class DependencyInjection
{
    public static IServiceCollection AddWebAPIService(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<IClaimService, ClaimsService>();
        services.AddScoped<ITokenService, TokenService>();
        services.AddControllers().AddJsonOptions(opt =>
            opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        services.AddHealthChecks();
        services.AddHttpContextAccessor();
        //services.Configure<MailSetting>(configuration.GetSection(nameof(MailSetting)));  // tranfer data to an instance of MailSettings at runtime
        services.AddScoped<IValidator<CreateTrainingProgramViewModel>, CreateTrainingProgramValidation>();
        services.AddScoped<IValidator<UpdateTrainingProgramViewModel>, UpdateTrainingProgramValidation>();
        //---------------------------------------------------------------------------------------
        services.AddAuthentication(option =>
        {
            option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            option.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        })
            .AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8
                        .GetBytes(configuration.GetSection("Jwt:SecretKey").Value)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                };
            });
       /* services.AddScoped<MailService>();
        services.AddTransient<SendAttendanceMailJob>();*/
       /* services.AddQuartz(q =>
        {
            q.UseMicrosoftDependencyInjectionJobFactory();
            q.ScheduleJob<SendAttendanceMailJob>(job => job
                .WithIdentity("AttendanceMailJob")
                .WithDescription("Sends attendance report emails at 11:30 am every day.")
                .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(11, 30))
            );
        });*/

      /*  services.AddQuartzHostedService(options => options.WaitForJobsToComplete = true);*/
        /*services.AddAuthorization(opt =>
        {
            // set Policy
            opt.AddPolicy("AuthUser", policy => policy.RequireAuthenticatedUser()); // only user already login
            opt.AddPolicy("All", policy => policy.RequireRole("SuperAdmin","ClassAdmin","Trainer","Student","Mentor","Auditor"));
            opt.AddPolicy("OnlySupperAdmin",policy => policy.RequireRole("SuperAdmin"));
            opt.AddPolicy("Admins", policy => policy.RequireRole("SuperAdmin", "ClassAdmin"));
            opt.AddPolicy("Audits", policy => policy.RequireRole("SuperAdmin", "Auditor"));
            opt.AddPolicy("AuditResults", policy => policy.RequireRole("SuperAdmin", "Auditor", "Mentor", "Trainer"));
        });*/
        //-------------------------------------------------------------------------------------------
        return services;
    }
}
