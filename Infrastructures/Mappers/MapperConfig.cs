using Applications.ViewModels.UserViewModels;
using AutoMapper;
using Domain.Entities;
using Domain.EntityRelationship;
using Applications.Commons;
using Infrastructures.Mappers.UserMapperResovlers;
using Applications.ViewModels.ClassViewModels;
using Applications.ViewModels.TrainingProgramModels;
using Application.ViewModels.TrainingProgramModels;
using Applications.ViewModels.ClassTrainingProgramViewModels;

namespace Infrastructures.Mappers
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            /* pagination */
            CreateMap(typeof(Pagination<>), typeof(Pagination<>));
            /*View model mapper*/
            CreateMap<UserViewModel, User>();
            CreateMap<User, UserViewModel>()
                .ForMember(dest => dest.Gender, src => src.MapFrom(s => s.Gender == true ? "Male":"Female"))
                .ForMember(dest => dest.createByEmail, src => src.MapFrom<CreateByResolver>());
            CreateMap<UpdateUserViewModel, User>()
                .ForMember(dest => dest.Image, src => src.MapFrom<UpdateImageResovler>());
            CreateMap<ClassViewModel, Class>().ReverseMap();
            CreateMap<CreateUserViewModel, ClassUser>().ReverseMap();
            CreateMap<CreateTrainingProgramViewModel, TrainingProgram>().ReverseMap();
            CreateMap<TrainingProgramViewModel, TrainingProgram>().ReverseMap();
            CreateMap<UpdateTrainingProgramViewModel, TrainingProgram>().ReverseMap();
            CreateMap<ClassTrainingProgramViewModel, ClassTrainingProgram>().ReverseMap();
            CreateMap<CreateClassTrainingProgramViewModel, ClassTrainingProgram>().ReverseMap();
        }
    }
}
