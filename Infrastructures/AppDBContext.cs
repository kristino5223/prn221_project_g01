﻿using Domain.Entities;
using Domain.EntityRelationship;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Infrastructures
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

        // DbSet<>
     
        public DbSet<User> Users { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<ClassUser> ClassUser { get; set; }
        public DbSet<TrainingProgram> TrainingPrograms { get; set; }
        public DbSet<ClassTrainingProgram> ClassTrainingProgram { get; set; }
        public DbSet<Syllabus> Syllabuses { get; set; }
        public DbSet<OutputStandard> OutputStandards { get; set; }
        public DbSet<SyllabusOutputStandard> SyllabusOutputStandard { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Domain.Entities.File> File { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            base.OnModelCreating(modelBuilder);
        }

    }
}
