﻿using Applications.Interfaces;
using Applications.Services;
using Applications;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Applications.Repositories;
using Infrastructures.Repositories;
using Applications.IRepositories;
using Application.Repositories;
using Infrastructure.Repositories;
using Application.Interfaces;
using Application.Services;

namespace Infrastructures
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructureServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICurrentTime, CurrentTime>();
            // local; DBName: PRN221_PROJECT_G01
            services.AddDbContext<AppDBContext>(options => options.UseSqlServer(config.GetConnectionString("AppDB")));
            // Add Object Services
            services.AddScoped<IClassService, ClassService>();
            services.AddScoped<IClassRepository, ClassRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IClassUserRepository, ClassUserRepository>();
            services.AddScoped<IClassUserServices, ClassUserService>();
            services.AddScoped<ITrainingProgramRepository, TrainingProgramRepository>();
            services.AddScoped<ITrainingProgramService, TrainingProgramService>();
            services.AddScoped<IClassTrainingProgramRepository, ClassTrainingProgramRepository>();
            services.AddScoped<IClassTrainingProgramService, ClassTrainingProgramService>();

            return services;
        }
    }
}
