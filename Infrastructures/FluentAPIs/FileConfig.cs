﻿
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class FileConfig : IEntityTypeConfiguration<Domain.Entities.File>
    {
        public void Configure(EntityTypeBuilder<Domain.Entities.File> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne<Subject>(s => s.Subject)
                .WithMany(s => s.Files)
                .HasForeignKey(fk => fk.SubjectId);
        }
    }
}
