﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructures.FluentAPIs
{
    public class UnitConfig : IEntityTypeConfiguration<Unit>
    {
        public void Configure(EntityTypeBuilder<Unit> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne<Syllabus>(s => s.Syllabus)
                 .WithMany(s => s.Units)
                 .HasForeignKey(fk => fk.SyllabusId);
        }
    }
}
