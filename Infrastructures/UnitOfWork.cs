﻿
using Application.Repositories;
using Applications;
using Applications.IRepositories;
using Applications.Repositories;

namespace Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBContext _appDBContext;
        private readonly IClassRepository _classRepository;
        private readonly IUserRepository _userRepository;
        private readonly IClassUserRepository _classUserRepository;
        private readonly ITrainingProgramRepository _trainingProgramRepository;
        private readonly IClassTrainingProgramRepository _classTrainingProgramRepository;

        public UnitOfWork(AppDBContext appDBContext,
            IClassRepository classRepository,
            IUserRepository userRepository,
            IClassUserRepository classUserRepository,
            ITrainingProgramRepository trainingProgramRepository,
            IClassTrainingProgramRepository classTrainingProgramRepository)
        {
            _appDBContext = appDBContext;
            _classRepository = classRepository;
            _userRepository = userRepository;
            _classUserRepository = classUserRepository;
            _trainingProgramRepository = trainingProgramRepository;
            _classTrainingProgramRepository = classTrainingProgramRepository;
        }
        public IUserRepository UserRepository => _userRepository;
        public IClassRepository ClassRepository => _classRepository;
        public IClassUserRepository ClassUserRepository => _classUserRepository;

        public ITrainingProgramRepository TrainingProgramRepository => _trainingProgramRepository;

        public IClassTrainingProgramRepository ClassTrainingProgramRepository => _classTrainingProgramRepository;

        public async Task<int> SaveChangeAsync() => await _appDBContext.SaveChangesAsync();
    }
}
