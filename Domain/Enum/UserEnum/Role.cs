﻿namespace Domain.Enum.RoleEnum
{
    public enum Role
    {
        Admin,
        Lecturer,
        Student
    }
}
