﻿
using Domain.Base;
using Domain.Enum.StatusEnum;

namespace Domain.Entities
{
    public class File : BaseEntity
    {
        public string Content { get; set; }
        public string? Note { get; set; }
        public Status status { get; set; }
        public Subject Subject { get; set; }
        public Guid SubjectId { get; set; }
    }
}
