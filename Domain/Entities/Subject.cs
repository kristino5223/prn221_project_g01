﻿using Domain.Base;
using Domain.Enum.StatusEnum;
using Domain.Enum.SubjectEnum;

namespace Domain.Entities
{
    public class Subject : BaseEntity
    {
        public string SubjectName { get; set; }
        public SubjectTypeEnum SubjectType { get; set; }
        public string? Description { get; set; }
        public double Duration { get; set; }
        public bool IsOnline { get; set; }
        public Status Status { get; set; }
        public Guid UnitId { get; set; }
        public Unit Unit { get; set; }
        public ICollection<File>? Files { get; set; }
    }
}
