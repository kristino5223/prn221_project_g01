﻿using Domain.Base;
using Domain.EntityRelationship;
using Domain.Enum.StatusEnum;

namespace Domain.Entities
{
    public class Syllabus : BaseEntity
    {
        public string SyllabusName { get; set; }
        public string SyllabusCode { get; set; }
        public double Duration { get; set; }
        public string? CourseObjective { get; set; }
        public string? TechicalRequirement { get; set; }
        public Status Status { get; set; }
        public TrainingProgram TrainingProgram { get; set; }
        public Guid TrainingProgramId { get; set; }
        public ICollection<Unit>? Units { get; set; }
        public ICollection<SyllabusOutputStandard>? SyllabusOutputStandards { get; set; }

    }
}
