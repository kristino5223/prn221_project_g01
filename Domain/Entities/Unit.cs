﻿using Domain.Base;
using Domain.Enum.StatusEnum;

namespace Domain.Entities
{
    public class Unit : BaseEntity
    {
        public string UnitName { get; set; }
        public string UnitCode { get; set; }
        public double Duration { get; set; }
        public Status Status { get; set; }
        public Syllabus Syllabus { get; set; }
        public Guid SyllabusId { get; set; }
        public ICollection<Subject>? Subjects { get; set; }
    }
}
