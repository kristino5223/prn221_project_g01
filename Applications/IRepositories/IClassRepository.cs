﻿using Applications.Commons;
using Applications.Repositories;
using Domain.Entities;

namespace Applications.IRepositories
{
    public interface IClassRepository : IGenericRepository<Class>
    {
        Task<Pagination<Class>> GetClassByName(string Name, int pageNumber = 0, int pageSize = 10);
        Task<Class> GetClassByClassCode(string ClassCode);
    }
}
