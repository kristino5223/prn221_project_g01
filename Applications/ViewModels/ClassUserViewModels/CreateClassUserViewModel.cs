﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Applications.ViewModels.ClassUserViewModels
{
    public class CreateClassUserViewModel
    {
        public Guid ClassId { get; set; }
        public Guid UserId { get; set; }
    }
}
