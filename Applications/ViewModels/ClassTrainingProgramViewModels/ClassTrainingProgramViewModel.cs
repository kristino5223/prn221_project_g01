﻿namespace Applications.ViewModels.ClassTrainingProgramViewModels
{
    public class ClassTrainingProgramViewModel
    {
        public Guid ClassId { get; set; }
        public Guid TrainingProgramId { get; set; }
        public DateTime CreationDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
