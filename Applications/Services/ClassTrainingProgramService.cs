﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels.ClassTrainingProgramViewModels;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using Domain.EntityRelationship;
using System.Net;

namespace Applications.Services
{
    public class ClassTrainingProgramService : IClassTrainingProgramService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ClassTrainingProgramService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<CreateClassTrainingProgramViewModel> AddTrainingProgramToClass(Guid ClassId, Guid TrainingProgramId)
        {
            try
            {
                var classOjb = await _unitOfWork.ClassRepository.GetByIdAsync(ClassId);
                var trainingProgram = await _unitOfWork.TrainingProgramRepository.GetByIdAsync(TrainingProgramId);

                if (classOjb != null && trainingProgram != null)
                {
                    var classTrainingProgram = new ClassTrainingProgram()
                    {
                        Class = classOjb,
                        TrainingProgram = trainingProgram
                    };
                    await _unitOfWork.ClassTrainingProgramRepository.AddAsync(classTrainingProgram);
                    var isSuccess = await _unitOfWork.SaveChangeAsync() > 0;
                    if (isSuccess)
                    {
                        return _mapper.Map<CreateClassTrainingProgramViewModel>(classTrainingProgram);
                    }
                }

                return null;
            }
            catch (Exception)
            {
                throw new ArgumentException("Error at AddTrainingProgramToClass");
            }
        }

        public async Task<Response> GetAllClassTrainingProgram(int pageIndex = 0, int pageSize = 10)
        {
            var classTrainingProgram = await _unitOfWork.ClassTrainingProgramRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<ClassTrainingProgramViewModel>>(classTrainingProgram);
          
            if (classTrainingProgram.Items.Count() < 1)
                return new Response(HttpStatusCode.NoContent, "No Class Training Program Found");
            else
                return new Response(HttpStatusCode.OK, "Search Succeed", result);
        }

        public async Task<CreateClassTrainingProgramViewModel> RemoveTrainingProgramFromClass(Guid ClassId, Guid TrainingProgramId)
        {
            try
            {
                var classTrainingProgram = await _unitOfWork.ClassTrainingProgramRepository.GetClassTrainingProgram(ClassId, TrainingProgramId);

                if (classTrainingProgram != null)
                {
                    _unitOfWork.ClassTrainingProgramRepository.SoftRemove(classTrainingProgram);
                    var isSucces = await _unitOfWork.SaveChangeAsync() > 0;
                    if (isSucces)
                    {
                        return _mapper.Map<CreateClassTrainingProgramViewModel>(classTrainingProgram);
                    }
                }

                return null;
            }
            catch (Exception)
            {
                throw new ArgumentException("Error at RemoveTrainingProgramToClass");
            }
        }
    }
}
