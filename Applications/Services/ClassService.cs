﻿using Applications.Commons;
using Applications.Interfaces;
using Applications.ViewModels.ClassViewModels;
using Applications.ViewModels.Response;
using AutoMapper;
using Domain.Entities;
using Domain.EntityRelationship;
using System.Net;

namespace Applications.Services
{
    public class ClassService : IClassService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public ClassService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Task<Response> CreateClass(CreateClassViewModel classDTO)
        {
            throw new NotImplementedException();
        }

        public async Task<Response> GetAllClasses(int pageIndex = 0, int pageSize = 10)
        {
            var classes = await _unitOfWork.ClassRepository.ToPagination(pageIndex, pageSize);
            var result = _mapper.Map<Pagination<ClassViewModel>>(classes);
            if (classes.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "No Class Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", result);
        }

        public async Task<Class>? GetClassByClassCode(string ClassCode)
        {
           var classes = await _unitOfWork.ClassRepository.GetClassByClassCode(ClassCode);
           var result = _mapper.Map<Class>(classes);
          
           return result;
        }

        public async Task<Response> GetClassById(Guid ClassId)
        {
            var classObj = await _unitOfWork.ClassRepository.GetByIdAsync(ClassId);
            var result = _mapper.Map<ClassViewModel>(classObj);
            if (result is null) return new Response(HttpStatusCode.NoContent, "No Class Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed", result);
        }

        public async Task<Response> GetClassByName(string Name, int pageIndex = 0, int pageSize = 10)
        {
            var classes = await _unitOfWork.ClassRepository.GetClassByName(Name, pageIndex, pageSize);
            var result = _mapper.Map<Pagination<ClassViewModel>>(classes);
            if (classes.Items.Count() < 1) return new Response(HttpStatusCode.NoContent, "No Class Found");
            else return new Response(HttpStatusCode.OK, "Search Succeed",result);
        }

        public Task<ClassViewModel> UpdateClass(Guid ClassId, CreateClassViewModel classDTO)
        {
            throw new NotImplementedException();
        }
    }
}
