﻿using Application.ViewModels.TrainingProgramModels;
using Applications.Commons;
using Applications.ViewModels.Response;
using Applications.ViewModels.TrainingProgramModels;

namespace Applications.Interfaces
{
    public interface ITrainingProgramService
    {
        Task<Response> ViewAllTrainingProgramAsync(int pageIndex = 0, int pageSize = 10);
        Task<Response> GetTrainingProgramByClassId(Guid ClassId, int pageIndex = 0, int pageSize = 10);
        Task<Response> GetTrainingProgramById(Guid TrainingProramId);
        Task<TrainingProgramViewModel>? CreateTrainingProgramAsync(CreateTrainingProgramViewModel TrainingProgramDTO);
        Task<UpdateTrainingProgramViewModel>? UpdateTrainingProgramAsync(Guid TrainingProgramId, UpdateTrainingProgramViewModel TrainingProgramDTO);
        Task<Response> GetByName(string name, int pageIndex = 0, int pageSize = 10);
       // Task<Response> GetTrainingProgramDetails(Guid TrainingProgramId);
    }
}
