﻿
using Applications.Commons;
using Applications.ViewModels.ClassViewModels;
using Applications.ViewModels.Response;
using Domain.Entities;

namespace Applications.Interfaces
{
    public interface IClassService
    {
        public Task<Response> CreateClass(CreateClassViewModel classDTO);
        public Task<Response> GetAllClasses(int pageIndex = 0, int pageSize = 10);
        public Task<Response> GetClassById(Guid ClassId);
        public Task<Class>? GetClassByClassCode(string classCode);
        public Task<Response> GetClassByName(string Name, int pageIndex = 0, int pageSize = 10);
        public Task<ClassViewModel> UpdateClass(Guid ClassId, CreateClassViewModel classDTO);
       //public Task<Pagination<ClassViewModel>> GetClassByFilter(ClassFiltersViewModel filters, int pageNumber = 0, int pageSize = 10);
       // public Task<ClassDetailsViewModel> GetClassDetails(Guid ClassId);
       //public Task<Response> UpdateStatusOnlyOfClass(Guid ClassId, UpdateStatusOnlyOfClass ClassDTO);
    }
}
