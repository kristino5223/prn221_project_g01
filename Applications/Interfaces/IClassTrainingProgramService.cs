﻿using Applications.ViewModels.ClassTrainingProgramViewModels;
using Applications.ViewModels.Response;

namespace Applications.Interfaces
{
    public interface IClassTrainingProgramService
    {

        Task<Response> GetAllClassTrainingProgram(int pageIndex = 0, int pageSize = 10);
        Task<CreateClassTrainingProgramViewModel> AddTrainingProgramToClass(Guid ClassId, Guid TrainingProgramId);
        Task<CreateClassTrainingProgramViewModel> RemoveTrainingProgramFromClass(Guid ClassId, Guid TrainingProgramId);
    }
}
