﻿
using Application.Repositories;
using Applications.IRepositories;
using Applications.Repositories;

namespace Applications
{
    public interface IUnitOfWork
    {
        public IUserRepository UserRepository { get; }
        public IClassRepository ClassRepository { get; }
        public IClassUserRepository ClassUserRepository { get; }
        public ITrainingProgramRepository TrainingProgramRepository { get; }
        public IClassTrainingProgramRepository ClassTrainingProgramRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
